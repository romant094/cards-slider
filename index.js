const ACTIVE_CLASS = 'active'
const SLIDE_CLASS = 'slide'
const slides = document.querySelectorAll(`.${SLIDE_CLASS}`)

const updateSlidesClasses = slideIndex => {
  slides.forEach((slide, index) => {
    if (slideIndex === index) {
      slide.classList.toggle(ACTIVE_CLASS)
    } else {
      slide.classList.remove(ACTIVE_CLASS)
    }
  })
}

slides.forEach((item, index) => {
  item.addEventListener('click', () => updateSlidesClasses(index))
})
